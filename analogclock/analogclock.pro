QT += widgets
QT += network


HEADERS       = analogclock.h

SOURCES       = analogclock.cpp \
                main.cpp

QMAKE_PROJECT_NAME = widgets_analogclock

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/widgets/analogclock
INSTALLS += target


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../sicemal/projetos/linphone-3.7.0/oRTP/build/win32native/release/ -loRTP
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../sicemal/projetos/linphone-3.7.0/oRTP/build/win32native/debug/ -loRTP
else:unix: LIBS += -L$$PWD/../../../../../../sicemal/projetos/linphone-3.7.0/oRTP/build/win32native/ -loRTP

INCLUDEPATH += $$PWD/../../../../../../sicemal/projetos/linphone-3.7.0/oRTP/build/win32native/Debug
DEPENDPATH += $$PWD/../../../../../../sicemal/projetos/linphone-3.7.0/oRTP/build/win32native/Debug

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../sicemal/projetos/linphone-3.7.0/oRTP/build/win32native/release/liboRTP.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../sicemal/projetos/linphone-3.7.0/oRTP/build/win32native/debug/liboRTP.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../sicemal/projetos/linphone-3.7.0/oRTP/build/win32native/release/oRTP.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../sicemal/projetos/linphone-3.7.0/oRTP/build/win32native/debug/oRTP.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../../sicemal/projetos/linphone-3.7.0/oRTP/build/win32native/liboRTP.a



